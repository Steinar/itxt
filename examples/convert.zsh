#! /bin/zsh

../itxt2txt.py -l 64 <linenoise.itxt >linenoise.txt
../itxt2html.py -sc <linenoise.itxt >linenoise.html "linenoise"
../itxt2groff.py -uL <linenoise.itxt | groff -Tutf8 -Dutf8 >linenoise.groff.txt
../itxt2groff.py -n <linenoise.itxt | groff -Tps -Dutf8 -ms >linenoise.ps
../litxt2html.py <simple.itxt >simple.html "simple"
