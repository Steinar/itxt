#! /usr/bin/env python3

# Copyright 2019-2022 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

import sys, getopt, shutil

help_text = """itxt2txt [-b] [-h] [-l columns]

-b
    ignore handling break markers, "_|", and let them pass through as any other
    text
-h
    print this text
-L
    limited mode, does not support indented paragraphs outside preformatted
    blocks; implies -b
-l columns
    reformat to line length of columns instead of terminal width

Read plain text from stdin and format according to the following rules:
    - One Level of indent is four spaces in input _|
    - Pre-formatted blocks always start and end with a line of 80 consecutive
    "-" characters _|
    - Forced line break is inserted by a ending a line with "_|" _|
    - Indent where (#spaces % 4) != 0 is considered an error"""

MONO = "--------------------------------------------------------------------------------"
SPACE = " "
BREAK_MARKER = "_|"
INDENT = 4

def ismonomarker(line):
    return line.rstrip() == MONO

class OutlineFormatter:
    def __init__(self, lines, columns, handle_break_markers):
        self.lines = lines
        self.columns = columns
        self.handle_break_markers = handle_break_markers
    def get_indent(self, line):
        indent = 0
        for c in line:
            if c == SPACE:
                indent += 1
            else:
                break
        if (indent % INDENT) != 0:
            raise Exception("Indent not multiplum of INDENT constant.")
        return indent >> 2
    def write_to(self, output = sys.stdout):
        self.output = output
        self._formatparagraphs()
    def _formatparagraphs(self):
        currindent = 0
        currparagraph = []
        document = []
        for line in self.lines:
            words = line.split()
            previndent = currindent
            currindent = self.get_indent(line)
            if len(words) == 0:
                if len(currparagraph) > 0:
                    document.append((previndent, currparagraph))
                document.append((0, []))
                currparagraph = []
            elif self.handle_break_markers and words[-1].endswith(BREAK_MARKER):
                if previndent != currindent and len(currparagraph) > 0:
                    document.append((previndent, currparagraph))
                    currparagraph = []
                if len(words[-1]) > len(BREAK_MARKER):
                    words[-1] = words[-1][:-len(BREAK_MARKER)]
                else:
                    del words[-1]
                currparagraph.extend(words)
                if len(currparagraph) == 0:
                    document.append((0, currparagraph))
                else:
                    document.append((currindent, currparagraph))
                currparagraph = []
            elif previndent != currindent:
                if len(currparagraph) > 0:
                    document.append((previndent, currparagraph))
                currparagraph = words
            else:
                currparagraph.extend(words)
        if len(currparagraph) > 0:
            document.append((currindent, currparagraph))
        for indent, paragraph in document:
            self._outputparagraph(paragraph, " " * indent * 4)
    def _outputparagraph(self, words, indent):
        if len(words) == 0:
            self.output.write("\n")
            return
        tmp_line = [indent + words[0]]
        curr_len = len(tmp_line[0])
        new_lines = []
        for word in words[1:]:
            if len(word) + 1 + curr_len <= self.columns:
                tmp_line.append(word)
                assert len(tmp_line) > 1
                curr_len = curr_len + len(word) + 1
            else:
                tmp_line[-1] = tmp_line[-1] + '\n'
                new_lines.append(' '.join(tmp_line))
                tmp_line = [indent + word]
                curr_len = len(tmp_line[0])
        if len(tmp_line) > 0:
            tmp_line[-1] = tmp_line[-1] + '\n'
            new_lines.append(' '.join(tmp_line))
        for line in new_lines:
            self.output.write(line)

def flush(data, destination, line_length, handle_break_markers):
    formatter = OutlineFormatter(data, line_length, handle_break_markers)
    formatter.write_to(destination)
    data.clear()

def write_monomarker(destination, line_length):
    destination.write("-" * line_length + "\n")

def reformat(source, destination, line_length, handle_break_markers,
        limited_itxt=False):
    in_mono = False
    reflow = []

    for line in source:
        if in_mono:
            if ismonomarker(line):
                write_monomarker(destination, line_length)
                in_mono = False
                assert len(reflow) == 0
            else:
                destination.write(line)
        else:
            if ismonomarker(line):
                in_mono = True
                if len(reflow) > 0:
                    flush(reflow, destination, line_length,
                            handle_break_markers)
                write_monomarker(destination, line_length)
            else:
                if limited_itxt and len(line.strip()) > 0 and line[0].isspace():
                    raise ValueError("Indents outside monospace not supported.")
                reflow.append(line)
    if len(reflow) > 0:
        flush(reflow, destination, line_length, handle_break_markers)
    assert len(reflow) == 0

def main():
    options, arguments = getopt.gnu_getopt(sys.argv[1:], "bhLl:")
    line_length = shutil.get_terminal_size()[0]
    handle_break_markers = True
    limited_itxt = False
    for name, value in options:
        if name == "-h":
            print(help_text)
            sys.exit(0)
        elif name == "-L":
            handle_break_markers = False
            limited_itxt = True
        elif name == "-l":
            line_length = int(value)
        elif name == "-b":
            handle_break_markers = False
    reformat(sys.stdin, sys.stdout, line_length, handle_break_markers,
            limited_itxt=limited_itxt)

if __name__ == "__main__":
    main()
