#! /usr/bin/env python3

# Copyright 2019-2021 Steinar Knutsen
#
# Licensed under the EUPL, Version 1.2 or - as soon they will be approved by the
# European Commission - subsequent versions of the EUPL (the "Licence"); You may
# not use this work except in compliance with the Licence. You may obtain a copy
# of the Licence at:
#
# https://joinup.ec.europa.eu/collection/eupl/eupl-text-eupl-12
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the Licence is distributed on an "AS IS" basis, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# Licence for the specific language governing permissions and limitations under
# the Licence.

import sys, getopt

help_text = """itxt2html [-b] [-c] [-h] [-s] <title>

-b
    treat break markers as any other text
-c
    add a small CSS and Javascript snippet to make for adjustable column width
-h
    print this text
-s
    strip the break and monospace markers from the generated HTML, if combined
    with -b, do not strip the break markers

Read plain text from stdin and generate HTML according to the following rules:
    - One Level of indent is four spaces in input _|
    - Pre-formatted blocks always start and end with a line of 80 consecutive
    "-" characters _|
    - Forced line break is inserted by a ending a line with "_|" _|
    - Indent where (#spaces % 4) != 0 is considered an error"""

title_missing = "Mandatory HTML document title missing, use -h for synopsis."

HEAD = """<!DOCTYPE html>
<html>
<head>
<style type="text/css">
body {
    font-family: sans-serif;
}
pre {
    font-family: monospace;
}
div.rightshift {
    margin-left: 2em;
}
div.padded {
    padding-top: 1em;
}
</style>
"""
BUTTONS = """<script>
function decreasePercentage(percentage) {
    if (percentage >= 30) {
        newPercentage = percentage - 10;
    } else if (percentage >= 10) {
        newPercentage = percentage - 5;
    } else {
        newPercentage = percentage - 1;
    }
    if (newPercentage <= 0) {
        newPercentage = 1;
    }
    return newPercentage;
}
function increasePercentage(percentage) {
    if (percentage >= 20) {
        newPercentage = percentage + 10;
    } else if (percentage >= 5) {
        newPercentage = percentage + 5;
    } else {
        newPercentage = percentage + 1;
    }
    return newPercentage;
}
let bodyWidth = 100;
function decreaseBodyWidth() {
    bodyWidth = decreasePercentage(bodyWidth);
    document.body.style.width = bodyWidth + '%';
    document.getElementById("displayBodyWidth").innerHTML = bodyWidth + '%';
}
function increaseBodyWidth() {
    bodyWidth = increasePercentage(bodyWidth);
    document.body.style.width = bodyWidth + '%';
    document.getElementById("displayBodyWidth").innerHTML = bodyWidth + '%';
}
let fontSize = 100;
function decreaseFontSize() {
    fontSize = decreasePercentage(fontSize);
    document.body.style.fontSize = fontSize + '%';
    document.getElementById("displayFontSize").innerHTML = fontSize + '%';
}
function increaseFontSize() {
    fontSize = increasePercentage(fontSize);
    document.body.style.fontSize = fontSize + '%';
    document.getElementById("displayFontSize").innerHTML = fontSize + '%';
}
</script>
<div>
<button type="button" onclick="decreaseFontSize();">ABC-</button>
<span id="displayFontSize">n%</span>
<button type="button" onclick="increaseFontSize();">ABC+</button>
<button type="button" onclick="decreaseBodyWidth();">|...|-</button>
<span id="displayBodyWidth">n%</span>
<button type="button" onclick="increaseBodyWidth();">|...|+</button>
<hr>
</div>
"""
BODY = "</head><body>\n"
FOOTER = "</body></html>\n"
TITLE_TEMPLATE = "<title>%s</title>"
MONO = "--------------------------------------------------------------------------------"
INDENT = 4
SPACE = " "
START_MONO = "<pre>\n"
END_MONO = "</pre>\n"
SHIFT_RIGHT = '<div class="rightshift">\n'
PADDING = '<div class="padded">\n'
PARA_END = "</div>\n"
BREAK_MARKER = "_|"
BREAK = "<br>\n"


quote_table = {
    '&': "&amp;",
    '<': "&lt;",
    '>': "&gt;"
}

def get_indent(line):
    indent = 0
    for c in line:
        if c == SPACE:
            indent += 1
        else:
            break
    if (indent % INDENT) != 0:
        raise Exception("Indent not multiplum of INDENT constant.")
    return indent >> 2

def cook(line):
    unwritten = i = 0
    buffer = []
    while i < len(line):
        subst = quote_table.get(line[i])
        if subst != None:
            if unwritten != i:
                buffer.append(line[unwritten:i])
            unwritten = i + 1
            buffer.append(subst)
        i += 1
    if unwritten < len(line):
        buffer.append(line[unwritten:])
    return "".join(buffer)

def ismonomarker(line):
    return line.rstrip() == MONO

def write_line(destination, line, in_mono, stripped_output, breaks_active):
    if not stripped_output:
        destination.write(cook(line))
        return

    if ismonomarker(line):
        return

    if in_mono:
        destination.write(cook(line))
    else:
        if breaks_active and line.rstrip().endswith(BREAK_MARKER):
            stripped = line.rstrip()[:-len(BREAK_MARKER)] + "\n"
            destination.write(cook(stripped))
        else:
            destination.write(cook(line))

def main(source, destination, title, stripped_output, breaks_active=False,
        widthbuttons=False):
    previndent = currindent = 0
    in_mono = False
    previous_line_empty = False
    suppress_vertical_padding = False
    padstack = []
    prevline = ""

    destination.write(HEAD)
    destination.write(TITLE_TEMPLATE % cook(title))
    destination.write(BODY)
    if widthbuttons:
        destination.write(BUTTONS)

    for line in source:
        if in_mono:
            write_line(destination, line, in_mono, stripped_output,
                    breaks_active)
            if ismonomarker(line):
                in_mono = False
                destination.write(END_MONO)
                suppress_vertical_padding = True
            prevline = line
        else:
            if len(line.strip()) == 0:
                previous_line_empty = True
                while len(padstack) > 0 and padstack[-1] == PADDING:
                    padstack.pop()
                    destination.write(PARA_END)
                prevline = line
                continue
            elif ismonomarker(line):
                in_mono = True
                for p in padstack:
                    destination.write(PARA_END)
                del padstack[:]
                destination.write(START_MONO)
                previndent = 0
                write_line(destination, line, in_mono, stripped_output,
                        breaks_active)
                prevline = line
                continue

            currindent = get_indent(line)
            if previndent > currindent:
                i = 0
                while i < (previndent - currindent):
                    destination.write(PARA_END)
                    removed = padstack.pop()
                    if removed == SHIFT_RIGHT:
                        i += 1
                # not really needed to check for vertical padding suppression
                # here, but do it for maintanability
                if previous_line_empty and not suppress_vertical_padding:
                    destination.write(PADDING)
                    padstack.append(PADDING)
            elif currindent > previndent:
                for i in range(currindent - previndent):
                    destination.write(SHIFT_RIGHT)
                    padstack.append(SHIFT_RIGHT)
                if previous_line_empty and not suppress_vertical_padding:
                    destination.write(PADDING)
                    padstack.append(PADDING)
            elif previous_line_empty and not suppress_vertical_padding:
                destination.write(PADDING)
                padstack.append(PADDING)
            elif breaks_active and prevline.rstrip().endswith(BREAK_MARKER):
                destination.write(BREAK)

            previous_line_empty = False
            prevline = line
            previndent = currindent
            suppress_vertical_padding = False
            write_line(destination, line, in_mono, stripped_output,
                    breaks_active)

    for p in padstack:
        destination.write(PARA_END)
    destination.write(FOOTER)

if __name__ == "__main__":
    stripped_output = False
    breaks_active = True
    widthbuttons = False
    options, arguments = getopt.gnu_getopt(sys.argv[1:], "bchs")
    for name, value in options:
        if name == "-b":
            breaks_active = False
        elif name == "-c":
            widthbuttons = True
        elif name == "-h":
            print(help_text)
            sys.exit(0)
        elif name == "-s":
            stripped_output = True
    if len(arguments) != 1:
            print(title_missing, file=sys.stderr)
            sys.exit(1)
    main(sys.stdin, sys.stdout, arguments[0], stripped_output, breaks_active,
            widthbuttons)
